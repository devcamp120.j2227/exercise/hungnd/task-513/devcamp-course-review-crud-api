// Khai báo thư viện mongoose
const mongoose = require("mongoose");
const { monitorEventLoopDelay } = require("perf_hooks");
// Khai báo model
const reviewModel = require('../model/reviewModel');
const courseModel = require('../model/courseModel');
const { updateCourseById } = require("./courseController");

// Create new review
const createReview = (req, res) => {
    //B1: Thu thập dữ liệu
    const courseId = req.params.courseId;
    const body = req.body;
    console.log(courseId);
    //63763b97ac01f7d86952973f
    console.log(body);
    // { bodyStars: 5, bodyNote: 'Good' }
    //B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(courseId)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "CourseID không hợp lệ"
        })
    }
    if (!(Number.isInteger(body.bodyStars) && body.bodyStars > 0 && body.bodyStars <= 5)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Rate không hợp lệ"
        })
    }
    //B3: Thao tác với CSDL
    const newReview = {
        _id: mongoose.Types.ObjectId(),
        stars: body.bodyStars,
        note: body.bodyNote
    }
    console.log(newReview);
    /*{
        _id: new ObjectId("637b998a5b6455dc09d55060"),
        stars: 4,
        note: 'Good'
    }*/
    reviewModel.create(newReview, (err, data) => {
        if (err) {
            return res.status(500).json({
                "status": "Error 500: internal server error",
                "message": err.message
            })
        }
        // Thêm ID của review mới được tạo vào trong mảng reviews của course truyền tại params
        courseModel.findByIdAndUpdate(courseId, {$push: {reviews: data._id}}, (err, updateCourseById) => {
            if (err) {
                return res.status(500).json({
                    "status": "Error 500: internal server error",
                    "message": err.message
                })
            }
            else {
                return res.status(201).json({
                    "status": "Create new courses successfully!",
                    "data": data
                })
            }
        })
    })
};

//get all review
const getAllReview = (req, res) => {
    //B1: Thu thập dữ liệu(không cần)
    //B2: Kiểm tra dữ liệu(không cần)
    //B3: Thực hiện load all review
    reviewModel.find((err, data) => {
        if (err) {
            return res.status(500).json({
                "status": "Error 500: internal server error",
                "message": err.message
            })
        }
        else {
            return res.status(201).json({
                "status": "Load all review successfully!",
                "data": data
            })
        }
    })
};

// get a review by id
const getReviewById = (req, res) => {
    //B1: Thu thập dữ liệu
    const reviewId = req.params.reviewId;
    //B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(reviewId)) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "Review Id is not valid!"
        })
    }
    // B3: Thực hiện load review theo id
    reviewModel.findById(reviewId, (err, data) => {
        if (err) {
            return res.status(500).json({
                "status": "Error 500: internal server error",
                "message": err.message
            })
        }
        else {
            return res.status(201).json({
                "status": "Get review by Id successfully!",
                "data": data
            })
        }
    })
};

// update review by Id
const updateReviewById = (req, res) => {
    //B1: Thu thập dữ liệu
    const reviewId = req.params.reviewId;
    console.log(reviewId);

    const body = req.body;
    console.log(body);

    //B2: Kiểm tra dữ liệu
    // KIểm tra course Id
    if (!mongoose.Types.ObjectId.isValid(reviewId)) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "Review Id is not valid!"
        })
    }
    if (!(Number.isInteger(body.bodyStars) && body.bodyStars > 0 && body.bodyStars <= 5)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Rate không hợp lệ"
        })
    }
    // B3: Thực hiện update review theo id 
    const updateReview = {}

    if(body.bodyStars !== undefined) {
        updateReview.stars = body.bodyStars
    }

    if(body.bodyNote !== undefined) {
        updateReview.note = body.bodyNote
    }

    reviewModel.findByIdAndUpdate(reviewId, updateReview, (err, data) => {
        if (err) {
            return res.status(500).json({
                "status": "Error 500: internal server error",
                "message": err.message
            })
        }
        else {
            return res.status(201).json({
                "status": "Update review by Id successfully!",
                "data": data
            })
        }
    })
};

// delete a review by Id
const deleteReviewById = (req, res) => {
    //B1: Thu thập dữ liệu
    const reviewId = req.params.reviewId;
    console.log(reviewId);
    // B2: Kiểm tra dữ liệu
    // KIểm tra course Id
    if (!mongoose.Types.ObjectId.isValid(reviewId)) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "Review Id is not valid!"
        })
    }
    // B3: Thực hiện xóa course theo id
    reviewModel.findByIdAndDelete(reviewId, (err, data) => {
        if (err) {
            return res.status(500).json({
                "status": "Error 500: internal server error",
                "message": err.message
            })
        }
        else {
            return res.status(201).json({
                "status": "Delete review by id successfully!",
                "data": data
            })
        }
    })
};

// Get all reiview of course
const getAllReviewOfCourse = (req, res) => {
    //B1: Thu thập dữ liệu
    const courseId = req.params.courseId;

    //B2: Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(courseId)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "CourseId is not valid!"
        })
    }

    //B3: Thực hiện khởi tạo dữ liệu
    courseModel.findById(courseId, (err, data) => {
        if (err) {
            return res.status(500).json({
                "status": "Error 500: internal server error",
                "message": err.message
            })
        }
        else {
            return res.status(201).json({
                "status": "Get all review of course successfully!",
                "data": data
            })
        }
    }).populate("reviews")
};
module.exports = {
    createReview,
    getAllReviewOfCourse,
    getAllReview,
    updateReviewById,
    getReviewById,
    deleteReviewById,
}