// Khai báo thư viện mongoose
const mongoose = require("mongoose");
// Khai báo model
const courseModel = require('../model/courseModel');

// create a courses
const createCourses = (req, res) => {
    //B1: Thu thập dữ liệu
    let bodyCourse = req.body;
    console.log(bodyCourse);
    //B2: Kiểm tra dữ liệu
    // Kiểm tra title
    if (!bodyCourse.title) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "Title is not valid!"
        });
    }
    // Kiểm tra noStudent
    if (!bodyCourse.noStudent || isNaN(bodyCourse.noStudent) || bodyCourse < 0) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "Number of Student is not valid!"
        })
    }
    //B3: thực hiện tạo mới course
    let newCourse = {
        _id: mongoose.Types.ObjectId(),
        title: bodyCourse.title,
        description: bodyCourse.description,
        noStudent: bodyCourse.noStudent
    }

    courseModel.create(newCourse, (err, data) => {
        if (err) {
            return res.status(500).json({
                "status": "Error 500: internal server error",
                "message": err.message
            })
        }
        else {
            return res.status(201).json({
                "status": "Create new courses successfully!",
                "data": data
            })
        }
    })
};

// get all courses
const getAllCourses = (req, res) => {
    //B1: Chuẩn bị dữ liệu
    //thu thập dữ liệu trên front-end
    let courseName = req.query.courseName;
    let minStudent = req.query.minStudent;
    let maxStudent = req.query.maxStudent;

    //tạo ra điều kiện lọc
    let condition = {};
    if (courseName) {
        condition.title = { $regex: courseName };
    }

    if (minStudent) {
        condition.noStudent = { $gte: minStudent };
    }

    if (maxStudent) {
        condition.noStudent = { ...condition.noStudent, $lte: maxStudent };
    }
    // hoặc dùng
    let condition2 = {
        title: courseName,
        $or: [
            { noStudent: { $lt: minStudent } },
            { noStudent: { $gt: maxStudent } }
        ]
    }
    // xem giá trị của biến đó trước khi đặt vào condition
    console.log(condition);

    //B2: Kiểm tra dữ liệu(không cần)
    //B3: Thực hiện tạo mới course
    courseModel.find(condition)
        .sort({title: "asc"}) // asc là tăng dần, desc là giảm dần
        .exec((err, data) => {
            if (err) {
                return res.status(500).json({
                    "status": "Error 500: internal server error",
                    "message": err.message
                })
            }
            else {
                return res.status(201).json({
                    "status": "Load all courses successfully!",
                    "data": data
                })
            }
        })
};

// get a course
const getCourseById = (req, res) => {
    //B1: Thu thập dữ liệu
    const coursesId = req.params.coursesId;
    //B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(coursesId)) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "Course Id is not valid!"
        })
    }
    // B3: Thực hiện load course theo id
    courseModel.findById(coursesId, (err, data) => {
        if (err) {
            return res.status(500).json({
                "status": "Error 500: internal server error",
                "message": err.message
            })
        }
        else {
            return res.status(201).json({
                "status": "Get courses by Id successfully!",
                "data": data
            })
        }
    })
};
// update course by Id
const updateCourseById = (req, res) => {
    //B1: Thu thập dữ liệu
    const coursesId = req.params.coursesId;
    console.log(coursesId);

    let bodyCourse = req.body;
    console.log(bodyCourse);

    //B2: Kiểm tra dữ liệu
    // KIểm tra course Id
    if (!mongoose.Types.ObjectId.isValid(coursesId)) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "Course Id is not valid!"
        })
    }
    // Kiểm tra title
    if (!bodyCourse.title) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "Title is not valid!"
        });
    }
    // Kiểm tra noStudent
    if (!bodyCourse.noStudent || isNaN(bodyCourse.noStudent) || bodyCourse < 0) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "Number of Student is not valid!"
        })
    }
    // B3: Thực hiện update course theo id
    let newCourse = {
        title: bodyCourse.title,
        description: bodyCourse.description,
        noStudent: bodyCourse.noStudent
    }
    courseModel.findByIdAndUpdate(coursesId, newCourse, (err, data) => {
        if (err) {
            return res.status(500).json({
                "status": "Error 500: internal server error",
                "message": err.message
            })
        }
        else {
            return res.status(201).json({
                "status": "Update courses by Id successfully!",
                "data": data
            })
        }
    })
};

// delete a course by Id
const deleteCourseById = (req, res) => {
    //B1: Thu thập dữ liệu
    const coursesId = req.params.coursesId;
    console.log(coursesId);
    // B2: Kiểm tra dữ liệu
    // KIểm tra course Id
    if (!mongoose.Types.ObjectId.isValid(coursesId)) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "Course Id is not valid!"
        })
    }
    // B3: Thực hiện xóa course theo id
    courseModel.findByIdAndDelete(coursesId, (err, data) => {
        if (err) {
            return res.status(500).json({
                "status": "Error 500: internal server error",
                "message": err.message
            })
        }
        else {
            return res.status(201).json({
                "status": "Delete courses by Id successfully!",
                "data": data
            })
        }
    })
}
module.exports = {
    getAllCourses,
    createCourses,
    getCourseById,
    updateCourseById,
    deleteCourseById
}
