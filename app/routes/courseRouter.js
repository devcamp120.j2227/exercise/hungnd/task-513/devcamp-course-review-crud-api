// khai báo thư viện express
const express = require('express');

// Import middleware
const {
    getAllCoursesMiddleware,
    getCoursesMiddleware,
    postCoursesMiddleware,
    putCoursesMiddleware,
    deleteCoursesMiddleware
} = require('../middleware/courseMiddleware');

// Tạo ra Router
const courseRouter = express.Router();

// Import course controller
const courseController = require('../controllers/courseController');

// dùng cho từng phương thức
courseRouter.get('/courses', getAllCoursesMiddleware, courseController.getAllCourses);

courseRouter.post('/courses', postCoursesMiddleware, courseController.createCourses);

courseRouter.put('/courses/:coursesId', putCoursesMiddleware, courseController.updateCourseById);

courseRouter.delete('/courses/:coursesId', deleteCoursesMiddleware, courseController.deleteCourseById);

courseRouter.get('/courses/:coursesId', getCoursesMiddleware, courseController.getCourseById);
module.exports = { courseRouter }