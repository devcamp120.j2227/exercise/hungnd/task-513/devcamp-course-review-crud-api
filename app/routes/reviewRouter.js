
// khai báo thư viện express
const express = require('express');

// Khai báo middleware
const {
    getAllReviewsMiddleware,
    getReviewsMiddleware,
    postReviewsMiddleware,
    putReviewsMiddleware,
    deleteReviewsMiddleware
} = require('../middleware/reviewMiddleware');

// Tạo ra Router
const reviewRouter = express.Router();

// Import reviewController 
const reviewController = require('../controllers/reviewController');

// get all review of course
reviewRouter.get('/courses/:courseId/reviews', getAllReviewsMiddleware, reviewController.getAllReviewOfCourse);

//get all review
reviewRouter.get('/reviews', getAllReviewsMiddleware, reviewController.getAllReview);

// create new review
reviewRouter.post('/courses/:courseId/reviews', postReviewsMiddleware, reviewController.createReview);

// update a review by id
reviewRouter.put('/reviews/:reviewId', putReviewsMiddleware, reviewController.updateReviewById);

// delete a review by id
reviewRouter.delete('/reviews/:reviewId', deleteReviewsMiddleware, reviewController.deleteReviewById);

// get a review by id
reviewRouter.get('/reviews/:reviewId', getReviewsMiddleware, reviewController.getReviewById);

module.exports = { reviewRouter }