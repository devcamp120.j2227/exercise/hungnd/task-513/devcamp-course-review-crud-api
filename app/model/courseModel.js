
// Khái báo thư viện mongooseJS
const mongoose = require("mongoose");
// khai báo class Schema
const Schema = mongoose.Schema;

// Khởi tạo 1 instance courseSchema từ class Schema
const courseSchema = new Schema({
    title: {
        type: String,
        required: true,
        unique: true
    },
    description: {
        type: String,
        required: false
    },
    noStudent: {
        type: Number,
        default: 0
    },
    // Một course có nhiều review
    reviews: [{
        type: mongoose.Types.ObjectId,
        ref: "Review"
    }]
    // Một course có 1 review thì bỏ kí tự []
}, {
    timestamps: true
});
module.exports = mongoose.model("Course", courseSchema);